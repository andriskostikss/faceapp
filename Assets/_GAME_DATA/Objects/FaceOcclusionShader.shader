﻿Shader "AR/FaceOcclusion"
{
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        Tags { "Queue" = "Geometry-1" }
        ZWrite On
        ZTest LEqual
        ColorMask 0

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float xPos : SV_Target;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

                float4 cameraPos = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1.0));
				if (cameraPos.x* 2 > -cameraPos.z){
					o.xPos = -v.vertex.x;
				}
				else if (cameraPos.x * 2 < cameraPos.z) {
					o.xPos = v.vertex.x;
				}
				else {
					o.xPos = 0;
				}
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				if (i.xPos < 0){
					discard;
				}
                return fixed4(1.0, 0.0, 0.0, 1.0);
            }
            ENDCG
        }
    }
}