﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;
using CI.HttpClient;
using System;
using System.Net.Mail;
using System.IO;
using System.Net;
using UnityEngine.Networking;

public class AppLogicScript : MonoBehaviour
{
    public GameObject session;
    private GameObject curSession;
    private int prevMask;
    public GameObject cam;
    public InputField emailF;
    public ARFaceManager manager;
    public List<GameObject> masks = new List<GameObject>();
    public List<GameObject> pages = new List<GameObject>();
    [Header("Mail sending")]
    [SerializeField]
    string partnerEmail = "info@overly.lv";
    [SerializeField]
    string partnerName;
    [SerializeField]
    string subject;
    [SerializeField, TextArea(3, 10)]
    string message;
    public GameObject resObj;
    public Text result;
    public Text resultText;
    public Text debugText;
    private int curPage = 0;
    private List<int> workScore = new List<int> { 0, 0, 0, 0 };
    private string folderPath;
    private string currentMail;


    /// <summary>
    /// Switches to next page
    /// </summary>
    public void Next()
    {
        // Checks if the page is not last 
        if (curPage != pages.Count - 1)
        {
            // deactivates cur page
            pages[curPage].SetActive(false);
            curPage++;
            // if page is second to last activates camera
            if (curPage == pages.Count - 2)
            {
                curSession = Instantiate(session);
                ShowResults();
            }
            // activates new page
            pages[curPage].SetActive(true);
        }
        else
        {
            Restart();
        }
    }
    /// <summary>
    /// Restarts session after last page
    /// </summary>
    private void RestartSession()
    {
        // Destroys session
        Destroy(curSession);
    }

    /// <summary>
    /// Restarts session after last page
    /// </summary>
    public void Restart()
    {
        // nulls all values
        RestartSession();
        currentMail = "";
        pages[curPage].SetActive(false);
        curPage = 0;
        pages[curPage].SetActive(true);
        for (int i = 0; i < workScore.Count; i++)
        {
            workScore[i] = 0;
        }
    }

    /// <summary>
    /// Gets called after end input on email field
    /// </summary>
    public void ChangeMail()
    {
        currentMail = emailF.text;
    }


    /// <summary>
    /// Makes Web request to Overly RESTAPI server to relay picture to inputed email
    /// </summary>
    void Upload()
    {
        HttpClient client = new HttpClient();
        MultipartFormDataContent fileData = new MultipartFormDataContent();
        byte[] img = File.ReadAllBytes(folderPath);

        string msg = "Täname vastamast! Loodame, et said oma tulevase karjääri valimiseks veidikene abi. \n Kui tahad olla 100% veendunud, milline amet just sulle sobivaim on, registreeri end karjäärinõustamisele ja leiame koos lahenduse! \n \n Karjäärinõustamine: \n https://www.tootukassa.ee/content/teenused/karjaarinoustamine \n\n Tööta ja õpi: \n https://www.tootukassa.ee/content/toota-ja-opi \n \n Ära unusta – tee mis tahad!";

        fileData.Add(new ByteArrayContent(img, "application/octet-stream"), "Filedata", Path.GetFileName(folderPath));

        debugText.text = Directory.Exists(Application.persistentDataPath) + " " + File.Exists(folderPath);


        client.Post(new Uri("http://overly.lv/email/email_srv.php?" +
            "my_email=" + currentMail +
            "&partner_email=" + partnerEmail +
            "&partner_name=" + partnerName +
            "&my_subject=" + subject +
            "&message=" + msg +
            "&my_password=boaztelem"
            ), fileData, HttpCompletionOption.AllResponseContent,
            (r) =>
            {
                try
                {
                    if (r.Data == null)
                    {
                        Debug.LogError("Email sending failed!");

                    }
                    else
                    {

                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex);
                }
            },
            (u) =>
            {
                //Debug.Log(u.PercentageComplete);
            });
        Next();
    }
    /// <summary>
    /// Intermediate method to call image upload
    /// </summary>
    public void SendEmail()
    {
        Upload();
    }
    /// <summary>
    /// Gets called when button "takescreenshot is pressed"
    /// makes app screenshot
    /// </summary>
    public void TakeScreenShot()
    {
        // Disables UI
        while (pages[curPage].activeSelf)
        {
            pages[curPage].SetActive(false);
        }
        // Generates unique id to filename 
        string myFileName = "Screenshot" + System.DateTime.Now.Hour + System.DateTime.Now.Minute + System.DateTime.Now.Second + ".png";
        folderPath = Application.persistentDataPath + "/" + myFileName;
        // Takes screenshot
        resObj.SetActive(true);
        ScreenCapture.CaptureScreenshot(myFileName);


        // Enables UI
        StartCoroutine(EnableUI());
    }
    /// <summary>
    /// waits half a second before turning on ui to compensate for takescreenshot method
    /// </summary>
    private IEnumerator EnableUI()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        resObj.SetActive(false);

        pages[curPage].SetActive(true);
        Next();
    }

    public void PrepareEmail()
    {
        // Triggers email sending
        SendEmail();
    }
    /// <summary>
    /// Shows result
    /// </summary>
    public void ShowResults()
    {

        // Lists Results
        List<int> dupes = new List<int>();
        List<int> maxDupes = new List<int>();
        result.text = "Your score is:\n" + "Social:" +
                        workScore[0] + "\nCreative:" +
                        workScore[1] + "\nInvestigative:" +
                        workScore[2] + "\nEnterprising:" +
                        workScore[3];


        // Sorts by dupes and Max value dupes
        for (int i = 0; i < workScore.Count; i++)
        {
            if (workScore.Contains(workScore[i]) && workScore.Max() != workScore[i])
            {
                dupes.Add(i);
            }
            if (workScore.Max() == workScore[i])
            {
                maxDupes.Add(i);
            }
        }


        int randomIndex = UnityEngine.Random.Range(0, dupes.Count);
        int max = workScore.Max();
        // Chooses mask
        if (dupes.Count > 0 && max <= workScore[dupes[randomIndex]])
        {
            while (prevMask == dupes[randomIndex])
            {
                randomIndex = UnityEngine.Random.Range(0, dupes.Count);
            }
            manager.facePrefab = masks[dupes[randomIndex]];
            prevMask = dupes[randomIndex];
        }
        else if (maxDupes.Count > 1)
        {
            while (prevMask == maxDupes[randomIndex])
            {
                randomIndex = UnityEngine.Random.Range(0, maxDupes.Count);
            }
            manager.facePrefab = masks[maxDupes[randomIndex]];
            prevMask = maxDupes[randomIndex];

        }
        else
        {
            randomIndex = workScore.IndexOf(max);
            manager.facePrefab = masks[randomIndex];
            prevMask = randomIndex;
        }

        int final = prevMask;
        switch (prevMask)
        {
            case 0:
                resultText.text = "Sa armastad inimkonda rohkem kui pagar kuklit ning jõuad kaugele, ümbritsetuna kõigist, kes sind selles julgustavad.";
                break;
            case 1:
                resultText.text = "Sa kõnetad nii väljakutseid kui tervet elu loovalt ja mänguliselt, tänu millele leiad rõõmu ka kõige nürimatest olukordadest.";
                break;
            case 2:
                resultText.text = "Su lemmikküsimus on konkrentsitult 'Kuidas?' – tänu entusiasmile kõike analüüsida lahendad sa hõlpsasti kõik juhtumid.";
                break;
            case 3:
                resultText.text = "Sinus on kamaluga julgust ja ettevõtlikkust, mis ei tunne piire – ära imesta kui end ühel hetkel kosmosest avastad.";
                break;
        }





    }
    /// <summary>
    /// Adds +1 score to workscore index num
    /// </summary>
    /// <param name="num">Parameter value to pass.</param>
    public void AddTo(int num)
    {
        switch (num)
        {
            case 1:
                workScore[0]++;
                break;
            case 2:
                workScore[1]++;

                break;
            case 3:
                workScore[2]++;

                break;
            case 4:
                workScore[3]++;

                break;
        }

        Next();
    }
}
